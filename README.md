# Игра шахматы

## Требуемые расширения
* php-redis
* php-json

## Установка
```
composer install
```

## Доступные команды
```
./bin/chessmate
```

### Создание шахматной доски
```
./bin/chessmate chess:create:json /tmp/chess.json 8 8
```

### Печать шахматной доски
```
./bin/chessmate chess:print:json /tmp/chess.json
```

### Установка короля на a:1
```
./bin/chessmate chess:set:json /tmp/chess.json a:1 king
```