<?php
namespace svmk\LocalInternet;
class Position {
	
    /**
     * $left возвращает позицию с лева
     *
     * @var integer
     *
     * @access protected
     */
	protected $left;

    /**
     * $bottom возвращает позицию с низу
     *
     * @var integer
     *
     * @access protected
     */
	protected $bottom;

    /**
     * __construct конструктор класса
     * 
     * @param integer $left   позиция с лева
     * @param integer $bottom позиция с низу
     *
     * @access public
     *
     */
	function __construct($left, $bottom) {
		$isInvalid = false;
		if (!preg_match('/^[a-z]{1,}$/isU',$left)) {
            $isInvalid = true;
        }
        $this->left = self::decodeBottom($left) + 1;
		$this->bottom = $bottom;
        if ($this->left <= 0) {
            $isInvalid = true;
        }
        if ($this->bottom <= 0) {
            $isInvalid = true;
        }
        if ($isInvalid) {
            throw new WrongPositionExcpetion('Неверный формат адреса на шахматной доске');
        }
	}

    /**
     * getLeftPosition возвращает позицию с лева
     * 
     * @access public
     *
     * @return integer
     */
	public function getLeftPosition() {
		return $this->left - 1;
	}

    /**
     * getBottomPosition возвращает позицию с права
     * 
     * @access public
     *
     * @return integer
     */
	public function getBottomPosition() {		
		return $this->bottom - 1;
	}

    /**
     * getKey возвращает ключ для хранилища
     * 
     * @access public
     *
     * @return string
     */
	public function getKey() {
		return $this->getLeftPosition().':'.$this->getBottomPosition();
	}

    /**
     * __toString приводит объект к строке
     * 
     * @access public
     *
     * @return string
     */
    public function __toString() {
        return self::encodeBottom($this->left - 1).':'.$this->bottom;
    }

    /**
     * fromString возвращает объект из строки
     * 
     * @param string $str строка
     *
     * @access public
     * @static
     *
     * @return Position
     */
    public static function fromString($str) {
        $parts = explode(':',$str);
        if (count($parts) != 2) {
            throw new WrongPositionExcpetion('Не верный формат позиции');
        }
        return new self($parts[0],$parts[1]);
    }

    /**
     * encodeBottom переводит число в строку
     * 
     * @param integer $n число
     *
     * @access public
     * @static
     *
     * @return string
     */
    public static function encodeBottom($n) {
        for($r = ""; $n >= 0; $n = intval($n / 26) - 1) {            
            $r = chr($n%26 + 0x61) . $r;
        }
        return $r;
    }

    /**
     * decodeBottom переводит строку в число
     * 
     * @param string $num строка
     *
     * @access public
     * @static
     *
     * @return integer
     */
    public static function decodeBottom($num) {
        $l = strlen($num);
        $n = 0;
        for($i = 0; $i < $l; $i++)
            $n = $n*26 + ord($num[$i]) - 0x60;
        return $n-1;
    }
}