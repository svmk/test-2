<?php
namespace svmk\LocalInternet\commands;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use svmk\LocalInternet\ChessMate;
use svmk\LocalInternet\Position;
class PrintChessCommand extends StorageCommand {
    
    /**
     * configure настраивает команду
     * 
     * @access protected
     *
     */
	protected function configure() {
        $this
            ->setName('chess:print')
            ->setDescription('Печатает шахматную доску')
        ;
    }

    /**
     * execute выполняет команду
     * 
     * @param InputInterface  $input  поток входа
     * @param OutputInterface $output поток выхода
     *
     * @access protected
     *
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        $connection = $this->getActiveStorage($input);
        $chessMate = new ChessMate($connection,$this->container->get('figures'));
        if (!$chessMate->isValid()) {
            throw new \Exception('Доска не существует. Создайте доску!');            
        }
        $leftStrLen = strlen($chessMate->getWidth());
        if ($leftStrLen <= 2) {
            $leftStrLen = 2;
        }
        $bottomStrLen = strlen($chessMate->getHeight());
        echo '|'.sprintf('%'.$bottomStrLen.'s','');
        foreach ($chessMate->getWidthIterator() as $left) {
            echo '|'.sprintf('%'.$leftStrLen.'s',$left);
        }
        echo '|'.PHP_EOL;
        foreach ($chessMate->getHeightIterator() as $bottom) {
            echo '|'.sprintf('%'.$bottomStrLen.'s',$bottom);
            foreach ($chessMate->getWidthIterator() as $left) {
                $figure = $chessMate->getFigure(new Position($left,$bottom));
                $figureText = null;
                if ($figure) {
                    $figureText = $figure->getShortTitle();
                }
                echo '|'.sprintf('%'.$leftStrLen.'s',$figureText);                
            }
            echo '|'.PHP_EOL;
        }
        echo '|'.sprintf('%'.$bottomStrLen.'s','');
        foreach ($chessMate->getWidthIterator() as $left) {
            echo '|'.sprintf('%'.$leftStrLen.'s',$left);
        }
        echo '|'.PHP_EOL;
    }
}