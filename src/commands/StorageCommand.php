<?php
namespace svmk\LocalInternet\commands;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use svmk\LocalInternet\storages\IStorage;
use Symfony\Component\Console\Application;

/**
* StorageCommand команда, которая использует хранилище
*/
class StorageCommand extends Command {

    /**
     * $storage хранилище
     *
     * @var IStorage
     *
     * @access private
     */
	private $storage;

	/**
	 * @inheritdoc
	 */
	public function appendToApplication(Application $application) {
		foreach ($this->container->get('storages') as $storage) {
			if ($storage instanceof IStorage) {
				$command = clone $this;
				$command->storage = $storage;
				$command->setDefinition([]);
				$storage->initializeCommand($command);
				$command->configure();
				$command->setName($command->getName().':'.$storage->getName());				
				$description = $command->getDescription();
				if ($description) {
					$command->setDescription(
						$description.' ('.$storage->getName().')'
					);					
				}
				$application->add($command);
			}
		}
	}

    /**
     * getActiveStorage возвращает активное подключение
     *  
     * @param  InputInterface $input входящие данные
     *
     * @access protected
     *
     * @return IStorage
     */
	protected function getActiveStorage($input) {
		return $this->storage->open($input);
	}
}