<?php
namespace svmk\LocalInternet\commands;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use svmk\LocalInternet\figures\IFigure;
class PrintFiguresCommand extends Command {
    
    /**
     * configure настраивает команду
     * 
     * @access protected
     *
     */
	protected function configure() {
        $this
            ->setName('figures:print')
            ->setDescription('Выводит список фигур')
        ;
    }

    /**
     * execute выполняет команду
     * 
     * @param InputInterface  $input  поток входа
     * @param OutputInterface $output поток выхода
     *
     * @access protected
     *
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        $figures = $this->container->get('figures');
        foreach ($figures as $figure) {
            if ($figure instanceof IFigure) {
                echo $figure->getName()."\t".$figure->getShortTitle().PHP_EOL;
            }
        }
    }
}