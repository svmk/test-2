<?php
namespace svmk\LocalInternet\commands;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use svmk\LocalInternet\ChessMate;
use svmk\LocalInternet\Position;
class GetChessCommand extends StorageCommand {
    
    /**
     * configure настраивает команду
     * 
     * @access protected
     *
     */
	protected function configure() {
        $this
            ->setName('chess:get')
            ->setDescription('Показывает какая фигура установлена на шахматную доску')
            ->addArgument('position', InputArgument::REQUIRED,'Позиция')
        ;
    }

    /**
     * execute выполняет команду
     * 
     * @param InputInterface  $input  поток входа
     * @param OutputInterface $output поток выхода
     *
     * @access protected
     *
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        $connection = $this->getActiveStorage($input);
        $chessMate = new ChessMate($connection,$this->container->get('figures'));
        if (!$chessMate->isValid()) {
            throw new \Exception('Доска не существует. Создайте доску!');            
        }
        $position = Position::fromString($input->getArgument('position'));
        $figure = $chessMate->getFigure($position);
        if ($figure) {
            echo $figure->getName().PHP_EOL;
        }
    }
}