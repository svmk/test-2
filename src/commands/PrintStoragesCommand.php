<?php
namespace svmk\LocalInternet\commands;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use svmk\LocalInternet\storages\IStorage;
class PrintStoragesCommand extends Command {
    
    /**
     * configure настраивает команду
     * 
     * @access protected
     *
     */
	protected function configure() {
        $this
            ->setName('storages:print')
            ->setDescription('Выводит список всех доступных хранилищ')            
        ;
    }

    /**
     * execute выполняет команду
     * 
     * @param InputInterface  $input  поток входа
     * @param OutputInterface $output поток выхода
     *
     * @access protected
     *
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        $storages = $this->container->get('storages');
        foreach ($storages as $storage) {
            if ($storage instanceof IStorage) {                
                $output->writeln($storage->getName()."\t".$storage->getDescription());
            }
        }
    }
}