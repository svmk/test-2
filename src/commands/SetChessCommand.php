<?php
namespace svmk\LocalInternet\commands;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use svmk\LocalInternet\ChessMate;
use svmk\LocalInternet\Position;
class SetChessCommand extends StorageCommand {
    
    /**
     * configure настраивает команду
     * 
     * @access protected
     *
     */
	protected function configure() {
        $this
            ->setName('chess:set')
            ->setDescription('Устанавливает фигуру на шахматную доску')
            ->addArgument('position', InputArgument::REQUIRED,'Позиция')
            ->addArgument('figure', InputArgument::REQUIRED,'Фигура')
        ;
    }

    /**
     * execute выполняет команду
     * 
     * @param InputInterface  $input  поток входа
     * @param OutputInterface $output поток выхода
     *
     * @access protected
     *
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        $connection = $this->getActiveStorage($input);
        $chessMate = new ChessMate($connection,$this->container->get('figures'));
        if (!$chessMate->isValid()) {
            throw new \Exception('Доска не существует. Создайте доску!');            
        }
        $position = Position::fromString($input->getArgument('position'));
        $figure = $chessMate->createFigure($input->getArgument('figure'));
        $chessMate->setFigure($position,$figure);
    }
}