<?php
namespace svmk\LocalInternet\commands;
use Symfony\Component\Console\Application;
abstract class Command extends \Symfony\Component\Console\Command\Command {
    /**
     * $container DI-контейнер
     *
     * @var DI\Container
     *
     * @access protected
     */
	protected $container;

    /**
     * setDIContainer устанавливает DI-контейнер
     * 
     * @param DI\Container $container DI-контейнер
     *
     * @access public
     *
     */
	public function setDIContainer($container) {
		$this->container = $container;
	}

    /**
     * appendToApplication подготавливает команду
     * 
     * @param  Symfony\Component\Console\Application $application приложение
     * @access public
     *
     */
    public function appendToApplication(Application $application) {
        $application->add($this);
    }
}