<?php
namespace svmk\LocalInternet\figures;
class Queen implements IFigure {
	/**
     * getName возвращает название фигуры
     * 
     * @access public
     *
     * @return string
     */
	public function getName() {
		return 'queen';
	}

    /**
     * getShortTitle возвращает короткое название
     * 
     * @access public
     *
     * @return string
     */
	public function getShortTitle() {
		return 'ф';
	}
}