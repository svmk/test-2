<?php
namespace svmk\LocalInternet\figures;
interface IFigure {
    /**
     * getName возвращает название фигуры
     * 
     * @access public
     *
     * @return string
     */
	public function getName();

    /**
     * getShortTitle возвращает короткое название
     * 
     * @access public
     *
     * @return string
     */
	public function getShortTitle();
}