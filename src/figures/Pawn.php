<?php
namespace svmk\LocalInternet\figures;
class Pawn implements IFigure {
	/**
     * getName возвращает название фигуры
     * 
     * @access public
     *
     * @return string
     */
	public function getName() {
		return 'pawn';
	}

    /**
     * getShortTitle возвращает короткое название
     * 
     * @access public
     *
     * @return string
     */
	public function getShortTitle() {
		return 'п';
	}
}