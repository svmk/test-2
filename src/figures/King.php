<?php
namespace svmk\LocalInternet\figures;
class King implements IFigure {
	/**
     * getName возвращает название фигуры
     * 
     * @access public
     *
     * @return string
     */
	public function getName() {
		return 'king';
	}

    /**
     * getShortTitle возвращает короткое название
     * 
     * @access public
     *
     * @return string
     */
	public function getShortTitle() {
		return 'Кр';
	}
}