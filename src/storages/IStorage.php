<?php
namespace svmk\LocalInternet\storages;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use svmk\LocalInternet\commands\StorageCommand;
interface IStorage {
    /**
     * getName возвращает название хранилища
     * 
     * @access public
     *
     * @return string
     */
	public function getName();

    /**
     * getDescription возвращает описание хранилища
     * 
     * @access public
     *
     * @return string
     */
	public function getDescription();

    /**
     * initializeCommand инициализирует настройки команды
     * 
     * @param svmk\LocalInternet\commands\StorageCommand $command команда
     *
     * @access public
     *
     */
     public function initializeCommand(StorageCommand $command);

    /**
     * open открывает соединение 
     * 
     * @param InputInterface $input входящие данные
     *
     * @access public
     *
     * @return IConnection
     */
     public function open(InputInterface $input);
}