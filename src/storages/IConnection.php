<?php
namespace svmk\LocalInternet\storages;
interface IConnection {
    
    /**
     * getValue возвращает значение
     * 
     * @param string $key ключ
     *
     * @access public
     *
     * @return string|null
     */
	public function getValue($key);

    /**
     * setValue устанавливает значение
     * 
     * @param string $key   ключ
     * @param string $value значение
     *
     * @access public
     *
     */
	public function setValue($key,$value);

    /**
     * removeValue удаляет значение
     * 
     * @param string $key   ключ
     *
     * @access public
     *
     */
    public function removeValue($key);

    /**
     * destroy очищает хранилище
     * 
     * @access public
     *
     */
    public function destroy();
}