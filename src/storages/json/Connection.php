<?php
namespace svmk\LocalInternet\storages\json;
use svmk\LocalInternet\storages\IConnection;
class Connection implements IConnection {

    /**
     * $file название хранилища
     *
     * @var string
     *
     * @access protected
     */
    protected $file;

    /**
     * __construct конструктор
     * 
     * @param string $file имя файла
     *
     * @access public
     *
     */
	function __construct($file) {
        $this->file = $file;
	}

    /**
     * getValue возвращает значение
     * 
     * @param string $key ключ
     *
     * @access public
     *
     * @return string|null
     */
    public function getValue($key) {
        $data = file_get_contents($this->file);
        $json = @json_decode($data,true);
        if (isset($json[$key])) {
        	return $json[$key];
        }
        return null;
    }

    /**
     * setValue устанавливает значение
     * 
     * @param string $key   ключ
     * @param string $value значение
     *
     * @access public
     *
     */
    public function setValue($key,$value) {
        $data = @file_get_contents($this->file);
        $json = @json_decode($data,true);
        if (!is_array($json)) {
        	$json = [];
        }
        $json[$key] = $value;
        file_put_contents($this->file,json_encode($json));
    }

    /**
     * removeValue удаляет значение
     * 
     * @param string $key   ключ
     *
     * @access public
     *
     */
    public function removeValue($key) {
        $data = file_get_contents($this->file);
        $json = @json_decode($data,true);
        if (!is_array($json)) {
        	$json = [];
        }
        unset($json[$key]);
        file_put_contents($this->file,json_encode($json));
    }

    /**
     * destroy очищает хранилище
     * 
     * @access public
     *
     */
    public function destroy() {
        @unlink($this->file);
    }
}