<?php
namespace svmk\LocalInternet\storages\json;
use svmk\LocalInternet\storages\IStorage;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use svmk\LocalInternet\commands\StorageCommand;

class Storage implements IStorage {
     
	/**
     * getName возвращает название хранилища
     * 
     * @access public
     *
     * @return string
     */
	public function getName() {
		return 'json';
	}

    /**
     * getDescription возвращает описание хранилища
     * 
     * @access public
     *
     * @return string
     */
	public function getDescription() {
		return 'Сохранение данных в json файл';
	}

    /**
     * initializeCommand инициализирует настройки команды
     * 
     * @param svmk\LocalInternet\commands\StorageCommand $command команда
     *
     * @access public
     *
     */
     public function initializeCommand(StorageCommand $command) {
          $command->addArgument(
               'file',
               InputArgument::REQUIRED,
               'Файл'
          );          
     }

     /**
     * open открывает соединение 
     * 
     * @access public
     *
     * @return IConnection
     */
     public function open(InputInterface $input) {
          $file = $input->getArgument('file');
          if (!is_file($file)) {                    
               @touch($file);
          }
          if (!is_writeable($file)) {
               throw new \Exception('Не удалось открыть файл');
          }
          return new Connection($file);
     }
}