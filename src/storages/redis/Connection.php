<?php
namespace svmk\LocalInternet\storages\redis;
use svmk\LocalInternet\storages\IConnection;
class Connection implements IConnection {
    /**
     * $redis соединение с redis
     *
     * @var Redis
     *
     * @access protected
     */
	protected $redis;

    /**
     * $name название хранилища
     *
     * @var string
     *
     * @access protected
     */
    protected $name;

    /**
     * __construct конструктор
     * 
     * @param Redis $redis подключение к redis
     *
     * @access public
     *
     */
	function __construct($redis,$name) {
		$this->redis = $redis;
        $this->name = $name;
	}

    /**
     * getValue возвращает значение
     * 
     * @param string $key ключ
     *
     * @access public
     *
     * @return string|null
     */
    public function getValue($key) {
        return $this->redis->get($this->name.'.'.$key);
    }

    /**
     * setValue устанавливает значение
     * 
     * @param string $key   ключ
     * @param string $value значение
     *
     * @access public
     *
     */
    public function setValue($key,$value) {
        $this->redis->set($this->name.'.'.$key,$value);
    }

    /**
     * removeValue удаляет значение
     * 
     * @param string $key   ключ
     *
     * @access public
     *
     */
    public function removeValue($key) {
        $this->redis->del($this->name.'.'.$key);
    }

    /**
     * destroy очищает хранилище
     * 
     * @access public
     *
     */
    public function destroy() {
        $keys = $this->redis->keys($this->name.'.*');
        foreach ($keys as $key) {
            $this->redis->del($key);
        }
    }
}