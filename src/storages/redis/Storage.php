<?php
namespace svmk\LocalInternet\storages\redis;
use svmk\LocalInternet\storages\IStorage;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use svmk\LocalInternet\commands\StorageCommand;

class Storage implements IStorage {
     
	/**
     * getName возвращает название хранилища
     * 
     * @access public
     *
     * @return string
     */
	public function getName() {
		return 'redis';
	}

    /**
     * getDescription возвращает описание хранилища
     * 
     * @access public
     *
     * @return string
     */
	public function getDescription() {
		return 'Подключение при помощи Redis';
	}

    /**
     * initializeCommand инициализирует настройки команды
     * 
     * @param svmk\LocalInternet\commands\StorageCommand $command команда
     *
     * @access public
     *
     */
     public function initializeCommand(StorageCommand $command) {
          $command->addArgument(
               'name',
               InputArgument::REQUIRED,
               'Название хранилища'
          );
          $command->addOption(
               'host',
               null,
               InputOption::VALUE_OPTIONAL,
               'Строка подключения',
               'localhost'
          );
     }

     /**
     * open открывает соединение 
     * 
     * @access public
     *
     * @return IConnection
     */
     public function open(InputInterface $input) {
          $redis = new \Redis();
          if (!$redis->open($input->getOption('host'))) {
               throw new \Exception('Не удалось подключиться к Redis');               
          }
          return new Connection($redis,$input->getArgument('name'));
     }
}