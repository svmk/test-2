<?php
namespace svmk\LocalInternet;
use svmk\LocalInternet\storages\IConnection;
use svmk\LocalInternet\figures\IFigure;
class ChessMate {

    /**
     * $connection соединение
     *
     * @var IConnection
     *
     * @access protected
     */
	protected $connection;

    /**
     * $figures фигуры
     *
     * @var array
     *
     * @access protected
     */
    protected $figures = [];

    /**
     * __construct 
     * 
     * @param IConnection $connection соединение
     *
     * @access public
     *
     */
	function __construct(IConnection $connection,$figures) {
		$this->connection = $connection;
        foreach ($figures as $figure) {
            if ($figure instanceof IFigure) {
                $this->figures[$figure->getName()] = $figure;
            }
        }
	}

    /**
     * getWidth возвращает ширину
     * 
     * @access public
     *
     * @return integer|null
     */
	public function getWidth() {
		return $this->connection->getValue('width');
	}

    /**
     * getHeight возвращает высоту
     * 
     * @access public
     *
     * @return integer|null
     */
	public function getHeight() {
		return $this->connection->getValue('height');	
	}

    /**
     * isValid проверяет валидна ли доска
     * 
     * @access public
     *
     * @return bool
     */
    public function isValid() {
        return $this->getWidth() > 0 && $this->getHeight() > 0;
    }

    /**
     * setWidth устанавливает ширину
     * 
     * @param integer $value значение
     *
     * @access public
     *
     */
	public function setWidth($value) {
		$this->connection->setValue('width',$value);	
	}

    /**
     * setHeight устанавливает высоту
     * 
     * @param integer $value значение
     *
     * @access public
     *
     */
	public function setHeight($value) {
		$this->connection->setValue('height',$value);		
	}

    /**
     * destroy разрушает шахматную доску
     * 
     * @access public
     *
     */
	public function destroy() {
		$this->connection->destroy();
	}

    /**
     * checkPosition проверяет позицию
     * 
     * @param Position $position позиция
     *
     * @access protected
     *
     */
    protected function checkPosition(Position $position) {
        $left = $position->getLeftPosition();
        $bottom = $position->getBottomPosition();
        $isInvalid = false;
        if ($left < 0 || $left >= $this->getWidth()) {
            $isInvalid = true;
        }
        if ($bottom < 0 || $bottom >= $this->getHeight()) {
            $isInvalid = true;   
        }
        if ($isInvalid) {            
            throw new \Exception('Позиция фигуры вне шахматной доски');            
        }
    }

    /**
     * getFigure возвращает фигуру
     * 
     * @param Position $position позиция
     *
     * @access public
     *
     * @return IFigure|null
     */
    public function getFigure(Position $position) {
        $this->checkPosition($position);
        $value = $this->connection->getValue($position->getKey());
        if (isset($this->figures[$value])) {
            return $this->figures[$value];
        }
        return null;
    }

    /**
     * removeFigure удаляет фигуру
     * 
     * @param Position $positioin позиция
     *
     * @access public
     *
     */
    public function removeFigure(Position $position) {
        $this->checkPosition($position);
        $this->connection->removeValue($position->getKey());
    }

    /**
     * setFigure устанавливает фигуру
     * 
     * @param Position $position позиция
     * @param IFigure  $figure   фигура
     *
     * @access public
     *
     */
    public function setFigure(Position $position,IFigure $figure) {
        $this->checkPosition($position);       
        $this->connection->setValue($position->getKey(),$figure->getName());
    }

    /**
     * getPositionIterator возвращает итератор по позициям
     * 
     * @access public
     *
     * @return Generator
     */
    public function getPositionIterator() {
        foreach ($this->getHeightIterator() as $bottom) {
            foreach ($this->getWidthIterator() as $left) {
                yield new Position($left,$bottom);
            }
        }        
    }

    /**
     * getWidthIterator возвращает итератор ширины
     * 
     * @access public
     *
     * @return Generator
     */
    public function getWidthIterator() {
        $width = $this->getWidth();
        for ($i = 1; $i <=  $width; $i++ ) {
            yield Position::encodeBottom($i - 1);
        }
    }

    /**
     * getHeightIterator возвращает итератор высоты
     * 
     * @access public
     *
     * @return Generator
     */
    public function getHeightIterator() {
        $height = $this->getHeight();
        for ($i = $height; $i >= 1; $i--) {
            yield $i;
        }
    }

    /**
     * clear очищает доску
     * 
     * @access public
     *
     */
    public function clear() {
        foreach ($this->getPositionIterator() as $position) {
            $this->removeFigure($position);
        }
    }

    /**
     * createFigure создаёт фигуру
     * 
     * @param string $name название фигуры
     *
     * @access public
     *
     * @return IFigure
     */
    public function createFigure($name) {
        if (isset($this->figures[$name])) {
            return $this->figures[$name];
        } else {
            throw new FigureNotFoundExcpetion('Фигура не найдена');            
        }
    }
}